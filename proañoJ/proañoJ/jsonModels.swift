//
//  jsonModels.swift
//  proañoJ
//
//  Created by JAIRO PROAÑO on 5/6/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import Foundation


struct Json1: Decodable {
    let viewTitle: String
    let date: String
    let nextLink: String
}

struct Json2: Decodable {
    let label: String
    let value: Int
}
