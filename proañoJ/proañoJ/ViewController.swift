//
//  ViewController.swift
//  proañoJ
//
//  Created by JAIRO PROAÑO on 5/6/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var date: UILabel!
    
    var jsonModel1: Json1?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getDataJSON()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! NextViewController
        
        destination.json1Model = jsonModel1
        destination.name = name.text
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getDataJSON() {
        let urlTorequest = "https://api.myjson.com/bins/72936"
        let url: URL! = URL(string: urlTorequest)
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (dataResume, urlResponse, error) in
            guard let  data = dataResume else {
                return
            }
            
            guard let json1Model = try? JSONDecoder().decode(Json1.self, from: data) else {
                return
            }
            
            DispatchQueue.main.async {
                self.jsonModel1 = json1Model
                self.viewTitle.text = "\(json1Model.viewTitle)"
                self.date.text = "\(json1Model.date)"
            }
        }
        task.resume()
    }

}

