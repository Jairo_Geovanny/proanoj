//
//  NextViewController.swift
//  proañoJ
//
//  Created by JAIRO PROAÑO on 5/6/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import UIKit

class NextViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var nameValue: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var average: UILabel!
    
    var json1Model: Json1?
    var name: String?
    
    var responseNextlink: [Json2]!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameValue.text = name
        
        getDataJSON()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if responseNextlink != nil {
            cell.textLabel?.text = "\(responseNextlink[indexPath.row].label)                   \(responseNextlink[indexPath.row].value)"
        } else {
            cell.textLabel?.text = "SD"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func getDataJSON() {
        let urlTorequest = json1Model?.nextLink
        let url: URL! = URL(string: urlTorequest!)
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (dataResume, urlResponse, error) in
            guard let  data = dataResume else {
                return
            }
            
            guard let json2Model = try? JSONDecoder().decode([Json2].self, from: data) else {
                return
            }
            
            DispatchQueue.main.async {
                self.responseNextlink = json2Model
                self.table?.reloadData()
                var sum = 0;
                for objectIn in self.responseNextlink {
                    sum += objectIn.value
                }
                let average = Double(sum) / Double(self.responseNextlink.count)
                
                self.average.text = "\(average)"
                
            }
        }
        task.resume()
    }
    
    
    
    
    
}
